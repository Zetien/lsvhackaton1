import juanp from "../assets/img/juan-lara.jpg";
import jorgez from "../assets/img/jorge-zetien.jpg";
import alvarov from "../assets/img/alvaro-vergara.jpg";
import "../App.css";

export function About() {
  return (
    <div className="container mt-2 mb-5">
      <h1 className="fs-1">HACKATON CARIV</h1>
      <h2 className="fs-2">Integrantes Grupo AJZ</h2>
      <div className="row">
        <div className="col d-flex justify-content-center">
          <a
            href="https://www.linkedin.com/in/juan-pablo-lara-angulo"
            target="_blank"
            style={{ textDecoration: "none" }}
          >
            <div className="card my-2">
              <img
                src={juanp}
                alt="Juan Pablo Lara Angulo"
                className="img-about-card"
              />
              Juan Pablo Lara Angulo
            </div>
          </a>
        </div>
        <div className="col d-flex justify-content-center">
          <a
            href="https://www.linkedin.com/in/jorge-luis-zetien-luna-674953149/"
            target="_blank"
            style={{ textDecoration: "none" }}
          >
            <div className="card my-2">
              <img
                src={jorgez}
                alt="Jorge Luis Zetien Luna"
                className="img-about-card"
              />
              Jorge Luis Zetien Luna
            </div>
          </a>
        </div>
        <div className="col d-flex justify-content-center">
          <a
            href="https://www.linkedin.com/in/%C3%A1lvaro-jos%C3%A9-vergara-garc%C3%ADa/"
            target="_blank"
            style={{ textDecoration: "none" }}
          >
            <div className="card my-2">
              <img
                src={alvarov}
                alt="Alvaro José Vergara"
                className="img-about-card"
              />
              Alvaro José Vergara
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}
