import { BannerHome } from "../components/banner/Banner-home";

export function Home() {
  return (
    <div className="">
      <div className="container mt-2 mb-5">
        <div className="row">
          <p className="mg-4 fs-4">
            Prueba de conocimientos adquiridos durante el CARIV, donde cada
            grupo realiza una actividad. Esto es solo es el comienzo...
          </p>
        </div>
      </div>
    </div>
  );
}
