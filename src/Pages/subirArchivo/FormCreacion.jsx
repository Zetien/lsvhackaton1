import React, { useState, useEffect } from "react";
import * as XLSX from "xlsx";

export function FormCreacion(props) {
  const readExcel = (file) => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsArrayBuffer(file);

      fileReader.onload = (e) => {
        const bufferArray = e.target.result;
        const wb = XLSX.read(bufferArray, { type: "buffer" });
        const wsName = wb.SheetNames[0];
        const ws = wb.Sheets[wsName];
        const data = XLSX.utils.sheet_to_json(ws);
        resolve(data);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
    promise.then((d) => {
      props.manejoItems(d);
    });
  };

  const readJson = (file) => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsText(file);

      fileReader.onload = (e) => {
        const data = JSON.parse(e.target.result);

        resolve(data);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
    promise.then((d) => {
      props.manejoItems(d);
    });
  };

  return (
    <div className="container">
      <h1>Formulario</h1>
      <div className="row">
        <div className="col-12 col-sm-10 col-md-8 col-lg-5 col-xl-6">
          <h5 className="pb-2">Selecione el tamanio del grupo</h5>
          <select
            onChange={(e) => {
              props.setTamanioGrupo(Number(e.target.value));
            }}
            className="form-select form-select mb-3"
            aria-label=".form-select-lg example"
          >
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
        <div className="col-12 col-sm-10 col-md-8 col-lg-5 col-xl-6">
          <h5 htmlFor="formFile" className="form-label pb-2">
            Agregar archivo
          </h5>
          <input
            className="form-control"
            type="file"
            onChange={(e) => {
              const file = e.target.files[0];
              console.log(file);
              if (file.name.endsWith(".xlsx")) {
                readExcel(file);
              } else if (file.name.endsWith(".json")) {
                readJson(file);
              }
              console.log(file);
            }}
          />
        </div>
        <div className="mt-3">
          <button
            type="button"
            onClick={props.crearGrupos}
            className="btn btn-primary"
          >
            generar grupos
          </button>
        </div>
      </div>
    </div>
  );
}
