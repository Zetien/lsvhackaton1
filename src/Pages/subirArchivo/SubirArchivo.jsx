import React, { useState } from "react";
import { FormCreacion } from "./FormCreacion";
import { CardArchivo } from "./CardArchivo";
import uuid4 from "uuid4";


export function SubirArchivo() {
  const [tamanioGrupo, setTamanioGrupo] = useState(2);
  const [items, setItems] = useState([]);
  const [grupos, setGrupos] = useState();

  function randomizer(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  function crearGrupos() {
    if (!items || items.length < 0) {
      return;
    }
    var myArray = [];
    const randomItems = randomizer([...items]);

    for (var i = 0; i < randomItems.length; i += tamanioGrupo) {
      const hasta = i + tamanioGrupo;
      const tmp =
        hasta >= randomItems.length
          ? randomItems.slice(i)
          : randomItems.slice(i, hasta);
      myArray.push(tmp);
    }

    setGrupos(myArray);
  }

  function manejoItems(datosItems) {
    setItems(datosItems);
  }
  return (
    <div className="container mt-2 mb-5">
      <div className="row">
        <h1 className="fs-1">HACKATON CARIV</h1>
        <p className="mg-4 fs-4">
          Carga un archivo al sistema en formato Excel o JSON para mostrar la
          lista de usuarios y oarticipantes del CAR IV
        </p>
        <div>
          <FormCreacion
            crearGrupos={crearGrupos}
            manejoItems={manejoItems}
            setTamanioGrupo={setTamanioGrupo}
          />
        </div>
        <div>
          {grupos?.map((grupo, index) => {
            console.log(grupos)
            return (
              <div key={uuid4()}>
                <h4 className="text-center">Grupo {index + 1}</h4>
                <div className="container">
                  <div className="row border p-3 my-4">
                    {grupo.map((item) => {
                      return (
                        <div className="col py-3 mx-1">
                          <CardArchivo key={uuid4()} {...item} />
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
