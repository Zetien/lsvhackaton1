import React from "react";
import images from '../../../assets/img/images.jsx'
export function CardArchivo(props) {
  return (
    <div className="col d-flex justify-content-center">
      <div className="card" style={{ fontSize: "1em" }}>
        <img
          src={images[props.Foto]}
          className="img-about-card"
          alt="card image cap"
          style={{ width: "85%" }}
          onError={(e) => {
            e.target.src =
              "https://previews.123rf.com/images/salamatik/salamatik1712/salamatik171200045/92143748-profile-anonymous-face-icon-gray-silhouette-person-male-default-avatar-photo-placeholder-isolated-on.jpg";
          }}
        />
        Cargo: {props.Cargo} <span></span>
        Nombre: {props.Nombres}
        {/* <ul className="list-group list-group-flush">
          <li className="list-group-item">Cargo: {props.Cargo}</li>
          <li className="list-group-item">Nombre: {props.Nombres} </li>
        </ul> */}
      </div>
    </div>
  );
}
