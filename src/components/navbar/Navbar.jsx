import { Link } from "react-router-dom";
import logo from "../../assets/img/logo-ajz.png";
export function Navbar() {
  return (
    <div className="nav-full">
      <div className="grad-bar"></div>
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light pt-1">
          <div className="container-fluid">
            <Link className="navbar-brand" to="/">
              <img src={logo} alt="Logo AJZ" />
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="nav navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link className="link-nav" to="/">
                    Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="link-nav" to="/SubirArchivo">
                    Subir Archivo
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="link-nav" to="/Pages/About">
                    Grupo de Trabajo
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div className="grad-bar"></div>
    </div>
  );
}
