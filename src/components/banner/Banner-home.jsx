import banner from "../../assets/img/banner-home.png";

export function BannerHome() {
  return <img src={banner} alt="banner hackaton CARIV" id='banner-home' />;
}
