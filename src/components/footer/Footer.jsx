import { Link } from "react-router-dom";

/* Site footer */

export function Footer() {
  return (
    <footer className="site-footer">
      <div className="container">
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <h6>Acerca de Nosotros</h6>
            <p className="text-justify foot">
              Somos un grupo formado por personal de las ciudades de Cartagena y
              Bogotá, trabajando en conjunto haciendo uso de las herramientas y
              habilidades adquiridos en el proceso de CAR IV. Con la ayuda de
              las tecnologías actuales como los son React, Javascrip y Bootstrap
              entre otras, este proyecto es una realidad.
            </p>
            <p>
              Intregrantes: Alvaro José Vergara - Jorge Luis Zetien Luna - Juan
              Pablo Lara Angulo
            </p>
          </div>

          {/* <div className="col-xs-6 col-md-3">
            <h6>Categories</h6>
            <ul className="footer-links">
              <li>
                <Link to="http://scanfcode.com/category/c-language/">C</Link>
              </li>
              <li>
                <Link to="http://scanfcode.com/category/front-end-development/">
                  UI Design
                </Link>
              </li>
              <li>
                <Link to="http://scanfcode.com/category/back-end-development/">
                  PHP
                </Link>
              </li>
              <li>
                <Link to="http://scanfcode.com/category/java-programming-language/">
                  Java
                </Link>
              </li>
              <li>
                <Link to="http://scanfcode.com/category/android/">Android</Link>
              </li>
              <li>
                <Link to="http://scanfcode.com/category/templates/">
                  Templates
                </Link>
              </li>
            </ul>
          </div> */}

          <div className="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul className="footer-links">
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/Pages/about/">Nosotros</Link>
              </li>
              <li>
                <Link to="/SubirArchivo">Subir Archivo</Link>
              </li>
              {/* <li>
                <Link to="http://scanfcode.com/privacy-policy/">
                  Privacy Policy
                </Link>
              </li>
              <li>
                <Link to="http://scanfcode.com/sitemap/">Sitemap</Link>
              </li> */}
            </ul>
          </div>
        </div>
        <hr />
      </div>
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-sm-6 col-xs-12">
            <p className="copyright-text">
              Copyright &copy; 2022 All Rights Reserved by <b>AJZ CAR-IV</b>.
            </p>
          </div>

          <div className="col-md-4 col-sm-6 col-xs-12">
            <ul className="social-icons">
              <li>
                <a
                  className="facebook"
                  href="https://www.facebook.com/lsvtechcolombia11/"
                  target="_blank"
                >
                  <i className="fa-brands fa-facebook-f"></i>
                </a>
              </li>
              <li>
                <a
                  className="instagram"
                  href="https://www.instagram.com/lsv_tech/"
                  target="_blank"
                >
                  <i className="fab fa-instagram"></i>
                </a>
              </li>
              <li>
                <a
                  className="twitter"
                  href="https://twitter.com/techlsv"
                  target="_blank"
                >
                  <i className="fa-brands  fa-twitter"></i>
                </a>
              </li>
              <li>
                <a
                  className="youtube"
                  href="https://www.youtube.com/channel/UC5gbK6qQflBM0fEObgEyYvw"
                  target="_blank"
                >
                  <i className="fa-brands fa-youtube"></i>
                </a>
              </li>
              <li>
                <a
                  className="linkedin"
                  href="https://co.linkedin.com/company/lsv-tech"
                  target="_blank"
                >
                  <i className="fa-brands  fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
}
