import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import { About } from "./Pages/About";
import { Footer } from "./components/footer/Footer";
import { Navbar } from "./components/navbar/Navbar";
import { Home } from "./Pages/Home";
import { SubirArchivo } from "./Pages/subirArchivo/SubirArchivo";
import { BannerHome } from "./components/banner/Banner-home";

function App() {
  return (
    <div className="App">
      <Navbar />
      <BannerHome />
      <Routes>
        <Route path="/Pages/About" element={<About />} />
        <Route path="/" element={<Home />} />
        <Route path="/SubirArchivo" element={<SubirArchivo />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
