import alanSanchez from './alan-sanchez.jpg';
import alvaroVergara from './alvaro-vergara.jpg';
import amandaQuintana from './amanda-quintana.jpg';
import andresGarcia from './andres-garcia.jpg';
import andresMolinares from './andres-molinares.jpg';
import angelicaMorales from './angelica-morales.jpg';
import danithBabilonia from './danith-babilonia.jpg';
import deiverVazquez from './deiver-vazquez.jpg';
import erickContreras from './erick-contreras.jpg';
import erickHerazo from './erick-herazo.jpg';
import EverNavarro from './ever-navarro.jpg';
import herlyCastillo from './herly-castillo.jpg';
import isaacBenavidez from './isaac-benavides.jpg';
import jairCalderon from './jair-calderon.jpg';
import jhonatanAlvarez from './jhonatan-alvarez.jpg';
import jorgeZetien from './jorge-zetien.jpg';
import juanLara from './juan-lara.jpg';
import juanPayares from './juan-payares.jpg';
import juanSierra from './juan-sierra.jpg';
import leandroMeza from './leandro-meza.jpg';
import luisPayares from './luis-payares.jpg';
import luisTilve from './luis-tilve.jpg';
import mariaAnaya from './maria-anaya.jpg';
import mauricioArias from './mauricio-ariaspng.jpg';
import oscarBallestas from './oscar-ballestas.jpg';
import ricardoJaramillo from './ricardo-jaramillo.jpg';
import ronaldPaternina from './ronald-paternina.jpg';
import steevenAltamiranda from './steeven-altamiranda.jpg';
import yilbertMolina from './yilber-molina.jpg';
import zuleydisBarrios from './zuleydis-barrios.jpg';

const images = {
  'assets/img/alan-sanchez.jpg': alanSanchez,
  'assets/img/alvaro-vergara.jpg': alvaroVergara,
  'assets/img/amanda-quintana.jpg': amandaQuintana,
  'assets/img/andres-molinares.jpg': andresMolinares,
  'assets/img/andres-garcia.jpg': andresGarcia,
  'assets/img/angelica-morales.jpg': angelicaMorales,
  'assets/img/danith-babilonia.jpg': danithBabilonia,
  'assets/img/deiver-vazquez.jpg': deiverVazquez,
  'assets/img/erick-contreras.jpg': erickContreras,
  'assets/img/erick-herazo.jpg': erickHerazo,
  'assets/img/ever-navarro.jpg': EverNavarro,
  'assets/img/herly-castillo.jpg': herlyCastillo,
  'assets/img/isaac-benavides.jpg': isaacBenavidez,
  'assets/img/jair-calderon.jpg': jairCalderon,
  'assets/img/jhonatan-alvarez.jpg': jhonatanAlvarez,
  'assets/img/jorge-zetien.jpg': jorgeZetien,
  'assets/img/juan-payares.jpg': juanPayares,
  'assets/img/juan-lara.jpg': juanLara,
  'assets/img/juan-sierra.jpg': juanSierra,
  'assets/img/leandro-meza.jpg': leandroMeza,
  'assets/img/luis-tilve.jpg': luisTilve,
  'assets/img/luis-payares.jpg': luisPayares,
  'assets/img/maria-anaya.jpg': mariaAnaya,
  'assets/img/mauricio-ariaspng.jpg': mauricioArias,
  'assets/img/oscar-ballestas.jpg': oscarBallestas,
  'assets/img/ricardo-jaramillo.jpg': ricardoJaramillo,
  'assets/img/ronald-paternina.jpg': ronaldPaternina,
  'assets/img/steeven-altamiranda.jpg': steevenAltamiranda,
  'assets/img/yilber-molina.jpg': yilbertMolina,
  'assets/img/zuleydis-barrios.jpg': zuleydisBarrios,
};

export default images;